# Value Stream Mapping

* [What is value stream mapping?](#introduction)
* [VSM wordbook](#how)
* [VSM metrics](#how)
* [Types of waste](#waste)


<h2><a name="introduction">What is value stream mapping?</a></h2>

Value stream mapping is a lean-management method for analyzing the current state and designing a future state for the series of events that take a product or service from its beginning through to the customer. At Toyota, it is known as "material and information flow mapping".[1] It can be applied to nearly any value chain.

See:
* [Wikipedia](https://en.wikipedia.org/wiki/Value_stream_mapping)
* [Value stream mapping for non-manufacturing by Martin](http://www.slideshare.net/AMEConnect/value-stream-mapping-for-non-manufacturingmartinreplacement)


<h2><a name="wordbook">VSM wordbook</a></h2>

* Value Adding (VA): any activity that your external customers value, and would be willing to pay for.

* Necessary Non-Value Adding (N NVA): any activity that is necessary but does not add value, for example any necessary support processes, legal regulatory requirements, etc.

* Unnessary Non-Value Adding (U NVA): any activity that is unnessary, a.k.a. waste.


<h2><a name="metrics">VSM metrics</a></h2>

In order of importance.

* Value Time (VT)

  * The time spent on adding value. This is actually performing the work.

* Process Time (PT)

  * A.k.a. touch time, work time, cycle time.

  * The time it takes to actually perform the work, if a person is able to work on it fully and focus on it fully.

  * Include task-specific doing, talking, thinking, etc.

* Lead Time (LT)

  * A.k.a. throughput time, turnaround time, elapsed time.

  * The elapsed time from when the input to a step is available, until the step is compeleted i.e. the work is sent along to the next step.

  * Include Process Time, not just the waiting time.

* Percentage Activity (%A)

  * Calculate (PT / LT) * 100

* Input Percentage Complete & Accurate (%C&amp;A)

  * The percentage of inputs that are fully usable, i.e. that are complete and that are accurate, as measured by the person receiving the inputs.

  * Similar to "first pass yield" in manufacturing.

* Rolled first pass yield (RFPY)

  * A.k.a. %Clean, %C&amp;A for stream

  * Calculate %C&amp;A * %C&amp;A * %C&amp;A ...

  * The percent of value stream output that goes through the process cleanly, i.e. without any need for rework.

* Number of Handoffs (#HO)

* Number of IT Systems (#IT)

* Freed capacity


<h2><a name="waste">Types of waste</a></h2>

* Overproduction

* Inventory

* Waiting

* Over-processing

* Errors

* Motion (people)

* Transportation (material, data, etc.)

* Underutilized people
